/*
 * DEMO 1
 * Author: Keiji Maeda
 * date: 23 / oct / 2017
 * sample: app notification
 * reference: https://developer.android.com/training/notify-user/build-notification.html
 **/

package com.example.keiji.k_demo_notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    // notification instance
    NotificationCompat.Builder notification;

    // unique id for the notification
    private static final int uniqueID = 12345;


    /**
     * CONSTRUCTOR
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize notification obj with this context
        notification = new NotificationCompat.Builder(this);

        // set auto cancel to true to clear the notification once you open your app
        notification.setAutoCancel(true);

    }//--end of onCreate METHOD


    /**
     * Sends notification to the main system
     * @param view
     */
    public void mySendNotificationToSystem (View view) {

        // CUSTOMIZING NOTIFICATION
        // build the notification (add attributes)
        // set icon
        notification.setSmallIcon(R.mipmap.ic_launcher);

        // set ticker text
        notification.setTicker("This is the ticker");

        // set the time of the notification
        notification.setWhen(System.currentTimeMillis());

        // set the content of the notification
        // title
        notification.setContentTitle("Title of the msg goes here.");
        // body
        notification.setContentText("Body of the msg goes here");


        // SET WHAT HAPPENS WHEN THE NOTIFICATION GETS CLICKED
        // create the intent for the notification to call this activity
        Intent intent = new Intent(this, MainActivity.class);

        // use pending intent to give the phone access to this intent
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Set the intent
        notification.setContentIntent(pendingIntent);


        // BUILDS NOTIFICATION AND SEND IT TO THE DEVICE
        // create the notification manager
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // call notify method passing the ID and the notification builded
        notificationManager.notify(uniqueID, notification.build());

    }//--end mySendNotificationToSystem METHOD

}//--END OF MAIN ACTIVITY
